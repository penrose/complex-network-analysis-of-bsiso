# -*- coding: utf-8 -*-
"""
Created on Sun Dec 12 20:58:22 2023

@author: gossing (CCA), gupta (corr_coeff, main)

"""


import math
import numpy as np
import pandas as pd
import random
import scipy.stats as st
import itertools
import multiprocessing as mp
import time
import timeit
import argparse
from scipy.linalg import eigh


#Import dataset

olr = np.load(olr_data)
wind = np.load(wind_data)

2d_data = np.array([olr, wind])

nodes_latlon = np.load(network_nodes)

'__________________________________________________'

def CCA(X,Y):
    """canonical correlation analysis - returns input data projected onto vector of their maximal covariance"""
    
    d,N = X.shape
    
    # center data
    Xc = X - X.mean(axis=1, keepdims = True)
    Yc = Y - Y.mean(axis=1, keepdims = True)
    
    # covariance metrices
    Cxx = np.dot(Xc,Xc.T) / N
    Cxy = np.dot(Xc,Yc.T) / N
    Cyx = np.dot(Yc,Xc.T) / N
    Cyy = np.dot(Yc,Yc.T) / N
    
    # S and D matrices ( S*w = lambda*D*w)
    S = np.block([
                    [Cxx*0,Cxy],
                    [Cyx,Cyy*0]
                    ])
    D = np.block([
                    [Cxx,Cxy*0],
                    [Cyx*0,Cyy]
                    ])

    eigvals,eigvecs = eigh(S, D)
    ind = np.argmax(eigvals)
    eigval = eigvals[ind]
    eigvec = eigvecs[:,ind]
    wx = eigvec[:d]
    wy = eigvec[d:]
    
    projX = wx.dot(X)
    projY = wy.dot(Y)
    
    return projX, projY



def corr_coeff(int_i, num_i):
	"""computes arrays containing correlation coefficients, to be concatenated to correlation matrix"""

    CorrMat = np.zeros((num_i, 2d_data.shape[1]))
    lag = 0
    alpha = 0.05

    for i in range(ind_i, min(2d_data.shape[1], ind_i + num_i)):
        for j in range(0, 2d_data.shape[1]):
            projX, projY = CCA(2d_data[:,i,:], 2d_data[:,j,:])
            corr, p = st.kendalltau(projX, projY)
            if p < alpha:
                CorrMat[i-ind_i, j] = corr
                
    np.save('safe_file_%d.npy' % (ind_i), CorrMat)         
    return 0


def main():
    parser = argparse.ArgumentParser(
        description='-----------------------test----------------------------')
    parser.add_argument('--exp_id', type=int, default=0)
    parser.add_argument('--num_i', type=int, default=51)
    args = parser.parse_args()
    exp_id = args.exp_id
    
    print(2d_data.shape[1])
    print(args.num_i)
    print(exp_id)
    
    if 2d_data.shape[1]-np.floor(2d_data.shape[1]/args.num_i)>0:
        num_limit = int(np.floor(2d_data.shape[1]/args.num_i))+1
    else:
        num_limit = int(np.floor(2d_data.shape[1] / args.num_i))

    ind_i = exp_id

    _res = corr_coeff(ind_i*args.num_i, args.num_i)


if __name__ == '__main__':
    main()


            
